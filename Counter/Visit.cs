namespace Counter
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Visit
    {
         public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public int? UserID { get; set; }

        public int? PageID { get; set; }
    }
}
