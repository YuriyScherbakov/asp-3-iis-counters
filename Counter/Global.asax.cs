﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Configuration;

namespace Counter
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            CounterModel counterModel = new CounterModel();
            counterModel.SaveChanges();
            Application ["CounterModel"] = counterModel;

            List<string> statAllowedPageList = new List<string>();
            
            foreach ( string str in
                System.IO.File.ReadAllLines(
                    Server.MapPath(ConfigurationManager.AppSettings ["statAllowedPageList"]) ))
            {
                statAllowedPageList.Add(str);
            }

            Application ["statAllowedPageList"] = statAllowedPageList;
            

        }

    }
}