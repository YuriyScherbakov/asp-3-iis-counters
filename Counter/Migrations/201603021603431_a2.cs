namespace Counter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class a2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Applics", "MyProperty", c => c.Int());
            AlterColumn("dbo.Applics", "MyProperty1", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Applics", "MyProperty1", c => c.Int(nullable: false));
            AlterColumn("dbo.Applics", "MyProperty", c => c.Int(nullable: false));
        }
    }
}
