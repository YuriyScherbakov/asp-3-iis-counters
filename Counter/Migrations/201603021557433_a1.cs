namespace Counter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class a1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Class2",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Applics", "MyProperty1", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "UserName12", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "UserName12");
            DropColumn("dbo.Applics", "MyProperty1");
            DropTable("dbo.Class2");
        }
    }
}
