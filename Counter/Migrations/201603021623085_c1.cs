namespace Counter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class c1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "UserName1");
            DropColumn("dbo.Users", "UserName12");
            DropColumn("dbo.Visits", "Time");
            DropTable("dbo.Applics");
            DropTable("dbo.Class1");
            DropTable("dbo.Class2");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Class2",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Class1",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Applics",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        MyProperty = c.Int(),
                        MyProperty1 = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.Visits", "Time", c => c.Time(precision: 7));
            AddColumn("dbo.Users", "UserName12", c => c.String(maxLength: 50));
            AddColumn("dbo.Users", "UserName1", c => c.String(maxLength: 50));
        }
    }
}
