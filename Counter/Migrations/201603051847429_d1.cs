namespace Counter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class d1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Visits", "Date", c => c.DateTime(nullable: false, storeType: "date"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Visits", "Date", c => c.DateTime(storeType: "date"));
        }
    }
}
