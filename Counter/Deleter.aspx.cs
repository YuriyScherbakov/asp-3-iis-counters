﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Counter
{
    public partial class Deleter : System.Web.UI.Page
    {
        protected void Page_Load(object sender,EventArgs e)
        {
            ( (CounterModel)Application ["CounterModel"] ).Database.ExecuteSqlCommand("DELETE FROM Visits");
            ( (CounterModel)Application ["CounterModel"] ).Database.ExecuteSqlCommand("DELETE FROM Users");
            ( (CounterModel)Application ["CounterModel"] ).SaveChanges();
          
        }
    }
}