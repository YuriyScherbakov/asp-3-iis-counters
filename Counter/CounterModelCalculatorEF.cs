﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counter
{
    
   public class CounterModelCalculatorEF
    {
        CounterModel counterModel;
        string currentPageName;

        public CounterModelCalculatorEF(CounterModel counterModel, string currentPageName)
        {
            this.counterModel = counterModel;
            this.currentPageName = currentPageName;
        }

        public int AllTimeRequestQuantity()
        {
            int res = counterModel.Visits.Count();
            return res;
        }
        public int ThisDayRequestQuantity()
        {
            int res = counterModel.Visits.Where(d => DbFunctions.TruncateTime(d.Date) == 
                                            DbFunctions.TruncateTime(DateTime.Now)).Count();
            return res;
         
        }
       
             public int  ThisPageRequestQuantity()
        {
            int res = ( counterModel.Visits.Where(p => p.PageID == ( (
             counterModel.Pages.Where(n => n.Name == currentPageName) ).FirstOrDefault().Id )).Count() );
            return res;
        }
        public int UniqeVisitorQuantity()
        {
            int res = counterModel.Users.Count();
            return res;
        }
        public int ThisDayVisitorQuantity()
        {
            int res = counterModel.Visits.Where(d => DbFunctions.TruncateTime(d.Date) == 
                                        DbFunctions.TruncateTime(DateTime.Now)).GroupBy(u => u.UserID).Count();
            return res;
        }


    }
}
