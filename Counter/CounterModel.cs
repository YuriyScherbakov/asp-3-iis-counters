namespace Counter
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CounterModel : DbContext
    {
        public CounterModel()
            : base("name=CounterDB")
        {
        }

        public virtual DbSet<PageName> Pages
        {
            get; set;
        }
        public virtual DbSet<User> Users
        {
            get; set;
        }
        public virtual DbSet<Visit> Visits
        {
            get; set;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
