﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Counter
{
    public partial class WebForm4 : System.Web.UI.Page
    {

        protected void Page_Load(object sender,EventArgs e)
        {
            CounterModelManager counterModelManager = new CounterModelManager();
            counterModelManager.PageResponse(this.Page);

            MasterFiller.Fill(this.Page);
        }

    }
}