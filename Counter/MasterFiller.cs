﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counter
{
    public class MasterFiller : System.Web.UI.Page
    {
        public static void Fill(System.Web.UI.Page currentPage)
        {
            if ( ( (List<string>)currentPage.Application ["statAllowedPageList"] ).Contains(currentPage.Request.Path) )
            {

                currentPage.Application.Lock();
                CounterModel counterModel = (CounterModel)currentPage.Application ["CounterModel"];
                currentPage.Application.UnLock();

                CounterModelCalculatorEF counterModelCalculatorEF = new CounterModelCalculatorEF(
                    counterModel,currentPage.Request.Path);

               ((SiteStatistics)currentPage.Master).LabelAllTimeRequestQuantity.Text = counterModelCalculatorEF.AllTimeRequestQuantity().ToString();
               ((SiteStatistics)currentPage.Master).LabelThisDayRequestQuantity.Text = counterModelCalculatorEF.ThisDayRequestQuantity().ToString();
               ((SiteStatistics)currentPage.Master).LabelThisDayVisitorQuantity.Text = counterModelCalculatorEF.ThisDayVisitorQuantity().ToString();
               ((SiteStatistics)currentPage.Master).LabelThisPageName.Text = " (" + currentPage.Request.Path + ") ";
               ((SiteStatistics)currentPage.Master).LabelThisPageRequestQuantity.Text = counterModelCalculatorEF.ThisPageRequestQuantity().ToString();
               ((SiteStatistics)currentPage.Master).LabelUniqeVisitorQuantity.Text = counterModelCalculatorEF.UniqeVisitorQuantity().ToString();
            }
            else
            {
                ((SiteStatistics)currentPage.Master).LabelAllTimeRequestQuantity.Text = "Статистика не отображается";
                ((SiteStatistics)currentPage.Master).LabelThisDayRequestQuantity.Text = "Статистика не отображается";
                ((SiteStatistics)currentPage.Master).LabelThisDayVisitorQuantity.Text = "Статистика не отображается";
                ((SiteStatistics)currentPage.Master).LabelThisPageName.Text = " (" + currentPage.Request.Path + ") ";
                ((SiteStatistics)currentPage.Master).LabelThisPageRequestQuantity.Text = "Статистика не отображается";
                ((SiteStatistics)currentPage.Master).LabelUniqeVisitorQuantity.Text = "Статистика не отображается";
            }
        }

    }
}
