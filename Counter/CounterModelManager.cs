﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counter
{
    public class CounterModelManager : System.Web.UI.Page
    {

        User currentUser;
        Visit currentVisit;
        string userName;
        PageName currentPageName;
        System.Web.UI.Page currentPage;

        public void PageResponse(System.Web.UI.Page currentPage)
        {
            this.currentPage = currentPage;
            GetCurrentPageName();
            GetOrCreateCurrentUser();
            CreateNewVisit();


            currentPage.Application.Lock();
            ( (CounterModel)currentPage.Application ["CounterModel"] ).SaveChanges();
            currentPage.Application.UnLock();
        }

        void CreateNewVisit()
        {
            currentVisit = new Visit()
            {
                Date = DateTime.Now,
                PageID = currentPageName.Id,
                UserID = currentUser.Id
            };
            currentPage.Application.Lock();
            ( (CounterModel)currentPage.Application ["CounterModel"] ).Visits.Add(currentVisit);
            currentPage.Application.UnLock();
        }
        void GetCurrentPageName()
        {
            currentPage.Application.Lock();
            currentPageName = ( (CounterModel)currentPage.Application ["CounterModel"] ).Pages.Where
                (val => val.Name == currentPage.Request.Path).FirstOrDefault();
            currentPage.Application.UnLock();
        }
        void GetOrCreateCurrentUser()
        {
            if ( currentPage.Request.Cookies ["ASP.NET_SessionId"] != null )
            {
                try
                {
                    userName = currentPage.Request.Cookies ["ASP.NET_SessionId"].Value;
                    currentUser = ( (CounterModel)currentPage.Application ["CounterModel"] ).Users.Where
                           (val => val.UserName == userName).First();

                }
                catch //Если юзер пришел с кукой, но в базе его нет - пусть становиться новым
                {
                    currentUser = new Counter.User()
                    {
                        UserName = currentPage.Request.Cookies ["ASP.NET_SessionId"].Value
                    };
                    currentPage.Application.Lock();
                    ( (CounterModel)currentPage.Application ["CounterModel"] ).Users.Add(currentUser);
                    ( (CounterModel)currentPage.Application ["CounterModel"] ).SaveChanges();
                    currentUser.Id =
                           ( (CounterModel)currentPage.Application ["CounterModel"] )
                           .Users.Where(u => u.UserName == currentUser.UserName).FirstOrDefault().Id;
                    currentPage.Application.UnLock();
                }
            }
            else
            {
                currentPage.Session ["dummy"] = 0;
                currentUser = new Counter.User()
                {
                    UserName = Session.SessionID
                };
                currentPage.Application.Lock();
                ( (CounterModel)currentPage.Application ["CounterModel"] ).Users.Add(currentUser);
                ( (CounterModel)currentPage.Application ["CounterModel"] ).SaveChanges();
                currentUser.Id =
                       ( (CounterModel)currentPage.Application ["CounterModel"] )
                       .Users.Where(u => u.UserName == currentUser.UserName).FirstOrDefault().Id;
                currentPage.Application.UnLock();

            }

        }

    }
}
