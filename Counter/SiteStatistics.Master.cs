﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Counter
{
    public partial class SiteStatistics : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender,EventArgs e)
        {

        }
        public Label LabelAllTimeRequestQuantity
        {
            get
            {
                return this.labelAllTimeRequestQuantity;
            }
        }
        public Label LabelThisDayRequestQuantity
        {
            get
            {
                return this.labelThisDayRequestQuantity;
            }
        }
        public Label LabelThisDayVisitorQuantity
        {
            get
            {
                return this.labelThisDayVisitorQuantity;
            }
        }
        public Label LabelThisPageName
        {
            get
            {
                return this.labelThisPageName;
            }
        }
        public Label LabelThisPageRequestQuantity
        {
            get
            {
                return this.labelThisPageRequestQuantity;
            }
        }

        public Label LabelUniqeVisitorQuantity
        {
            get
            {
                return this.labelUniqeVisitorQuantity;
            }
        }

        protected void Button1_Click(object sender,EventArgs e)
        {

        }
    }
}